# Matrix WP Rest Auth

A simple way for a Matrix homeserver to authenticate against the WP database of users in Wordpress. Simple, dirty.

You'll want to add some proper protections to prevent the WP server being easily bruteforced (the one-second sleep is minimal).

Installation:

You'll need to install and configure a working version of the python script which allows you to do REST authentication in your Matrix homeserver. Instructions here:

https://github.com/ma1uta/matrix-synapse-rest-password-provider

I didn't install with pip. I just ran this:

https://raw.githubusercontent.com/ma1uta/matrix-synapse-rest-password-provider/master/rest_auth_provider.py -o /opt/venvs/matrix-synapse/lib/python3.6/site-packages/rest_auth_provider.py
sudo sudo service matrix-synapse restart



Then, create a new web app somewhere and use these files. You'll need to provide Wordpress database creds in config.php.
The .htaccess should cause all requests to go to index.php - if you're not using Apache or don't support .htaccess, you'll be able to do this in your webserver. Let me know and I'll help.

The log file which is appended to will show you the authentication attempts - I recommend to disable this once you're set up.

Let me know through GitLab if you have any questions.


