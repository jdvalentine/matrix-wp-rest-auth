<?php

$ip = $_SERVER['REMOTE_ADDR'];

if (!in_array ($ip, $whitelist)) {

    die("You are $ip and you're not allowed");
}


require_once('init.php');
require_once('inc/class-phpass.php');


db_connect();

header('Content-Type: text/plain; charset=utf-8');

$path_only = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

$entityBody = file_get_contents('php://input');

$entityTree = $feed = json_decode($entityBody, true);


switch($path_only) {
    case '/_matrix-internal/identity/v1/check_credentials':
    sleep(1); // minimal brute-force protection

    $matrix_username = $entityTree['user']['id'];

    $matches = array();
    $t = preg_match('/@(.*?)\:/s', $matrix_username, $matches);

    $username = db_escape($matches[1]);

    $password = db_escape($entityTree['user']['password']);

    $wp_hasher = new PasswordHash( 8, TRUE );
    $hash = db_escape($wp_hasher->HashPassword( $password ));


    $q = "SELECT * FROM wp_users WHERE user_login = '$username'";
    $rs =  db_query($q);
    $r = mysqli_fetch_assoc($rs);

    if ($wp_hasher->CheckPassword($password, $r['user_pass'])) {
        $response['auth']['success'] = 'true';
        $response['auth']['mxid'] = $matrix_username;
        $response['auth']['profile']['display_name'] = $r['display_name'];
        $response['auth']['profile']['three_pids'] = array(array('medium' => 'email', 'address' => $r['user_email']));

    } else { // didn't auth correctly

        $response['auth']['success'] = 'false';
    
    }

    $json_response = json_encode($response);
    echo $json_response; // should let us in if the password hash matches

}

$log_file = '../logs/accesses.txt';

$log_str = date('d-m-Y H:i:s') . ": $ip    $username    $json_response\n";

file_put_contents($log_file, $log_str , FILE_APPEND );

